function multiplica(a, b)
	dima = size(a)
	dimb = size(b)
	if dima[2] != dimb[1]
		return -1
	end
	c = zeros(dima[1], dimb[2])
	for i in 1:dima[1]
		for j in 1:dimb[2]
			for k in 1:dima[2]
				c[i, j] = c[i, j] + a[i, k] * b[k, j]
			end
		end
	end
	return c
end

function matrix_pot(M, p)
	if p == 1
		return M
	else
		return multiplica(M, matrix_pot(M, p - 1))
	end
end

function matrix_pot_by_squaring(M, p)
	if p == 1
		return M
	elseif p % 2 == 0
		return matrix_pot_by_squaring(multiplica(M, M), p/2)
	else
		return multiplica(M, matrix_pot_by_squaring(multiplica(M, M), div(p, 2)))
	end
end
