# MAC0110-MiniEP9

This project is a Programming Exercise that focus on creating
algorithms, based on the Julia programming language, for calculating
the powers of a matrix. Also, getting more used to the Git mechanics
is an objective.
