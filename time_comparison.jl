include("miniep9.jl")
using LinearAlgebra

function compare_times()
	println("M = [1 2; 3 4], p = 2")
	M = [1 2; 3 4]
	@time matrix_pot(M, 2)
	@time matrix_pot_by_squaring(M, 2)
	println("M = [4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], p = 30")
	M = [4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7]
	@time matrix_pot(M, 7)
	@time matrix_pot_by_squaring(M, 7)
	println("M = [4 8 0 4 7 ; 8 4 9 6 4 ; 9 6 4 0 9 ; 9 5 4 7 2 ; 1 6 2 8 1], p = 200")
	M = [4 8 0 4 7 ; 8 4 9 6 4 ; 9 6 4 0 9 ; 9 5 4 7 2 ; 1 6 2 8 1]
	@time matrix_pot(M, 200)
	@time matrix_pot_by_squaring(M, 200)
	println("M = I_100x100, p = 10000")
	M = Matrix(LinearAlgebra.I, 100, 100)
	@time matrix_pot(M, 10000)
	@time matrix_pot_by_squaring(M, 10000)
end

compare_times()
