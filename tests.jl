include("miniep9.jl")
using Test

function quaseigual(n1, n2)
	if 0.99999 <= n1/n2 <= 1.00001
		return true
	else
		return false
	end
end

function testMatrix_pot()
	println("Início dos testes")
	@test matrix_pot([1 2; 3 4], 1) == [1 2; 3 4]
	@test matrix_pot([1 2; 3 4], 2) == [7.0 10.0; 15.0 22.0]
	M = matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)
	@test quaseigual(M[1, 1], 5.64284e8) && quaseigual(M[1, 2], 4.70889e8) && quaseigual(M[1, 3], 3.23583e8) && quaseigual(M[1, 4], 3.51859e8)
	@test quaseigual(M[2, 1], 8.31242e8) && quaseigual(M[2, 2], 6.93529e8) && quaseigual(M[2, 3], 4.76619e8) && quaseigual(M[2, 4], 5.18192e8)
	@test quaseigual(M[3, 1], 5.77004e8) && quaseigual(M[3, 2], 4.81473e8) && quaseigual(M[3, 3], 3.30793e8) && quaseigual(M[3, 4], 3.59677e8)
	@test quaseigual(M[4, 1], 7.99037e8) && quaseigual(M[4, 2], 6.66708e8) && quaseigual(M[4, 3], 4.58121e8) && quaseigual(M[4, 4], 4.98127e8)
	@test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)
	println("Fim dos testes")
end

testMatrix_pot()
